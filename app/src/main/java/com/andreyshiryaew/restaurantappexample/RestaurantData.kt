package com.andreyshiryaew.restaurantappexample

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Restaurant(
    @SerializedName("DeliveryCost")
    val DeliveryCost: Int,

    @SerializedName("DeliveryTime")
    val DeliveryTime: Int,

    @SerializedName("Logo")
    val Logo: String,

    @SerializedName("MinCost")
    val MinCost: Int,

    @SerializedName("Name")
    val Name: String,

    @SerializedName("PositiveReviews")
    val PositiveReviews: Int,

    @SerializedName("ReviewsCount")
    val ReviewsCount: Int,

    @SerializedName("Specializations")
    val Specializations: List<Specialization>
) : Serializable

data class Specialization (
    @SerializedName("Name")
    val Name: String
) : Serializable

