package com.andreyshiryaew.restaurantappexample

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.res.colorResource
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.andreyshiryaew.restaurantappexample.ui.screens.restaurant.RestaurantDetailsScreen
import com.andreyshiryaew.restaurantappexample.ui.screens.restaurant.RestaurantScreen
import com.andreyshiryaew.restaurantappexample.ui.theme.RestaurantAppExampleTheme

private var getListRestaurant: MutableList<Restaurant> by mutableStateOf(mutableListOf())
private var isLoading: Boolean by mutableStateOf(false)

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RestaurantAppExampleTheme {
                Scaffold(
                    backgroundColor = colorResource(id = R.color.white)
                ) {
                    Navigation()
                }
            }
        }

        val restaurantViewModel = ViewModelProvider(this)[RestaurantViewModel::class.java]
        restaurantViewModel.getRestaurantData().observe(this, Observer { it ->
            it?.let { ApiResponse ->
                when (ApiResponse.ApiStatus) {
                    ApiStatus.SUCCESS -> {
                        ApiResponse.data?.let {
                            getListRestaurant.clear()
                            getListRestaurant.addAll(it)
                            isLoading = false
                        }
                    }
                    ApiStatus.ERROR -> {
                        isLoading = false
                        Log.e("TAG", it.message.toString())
                    }
                    ApiStatus.LOADING -> {
                        isLoading = true
                    }
                }
            }
        })
    }
}

@Composable
fun Navigation() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = "main") {
        composable("main") {
            RestaurantScreen(navController = navController, listRestaurant = getListRestaurant, isLoading = isLoading)
        }
        composable("details/{restaurantName}", arguments = listOf(navArgument("restaurantName") { type = NavType.StringType })) { backStackEntry ->
            backStackEntry.arguments?.getString("restaurantName")?.let { restaurantName -> RestaurantDetailsScreen(restaurantName = restaurantName) }
        }
    }
}
