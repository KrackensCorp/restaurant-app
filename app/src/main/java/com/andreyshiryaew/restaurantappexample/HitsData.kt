package com.andreyshiryaew.restaurantappexample

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Hits(
    @SerializedName("ProductName")
    val ProductName: String,

    @SerializedName("ProductImage")
    val ProductImage: String,

    @SerializedName("ProductPrice")
    val ProductPrice: Int,

    @SerializedName("ProductDescription")
    val ProductDescription: String,

    @SerializedName("RestaurantId")
    val RestaurantId: Int,

    @SerializedName("RestaurantName")
    val RestaurantName: String,

    @SerializedName("RestaurantLogo")
    val RestaurantLogo: String

) : Serializable