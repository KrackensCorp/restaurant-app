package com.andreyshiryaew.restaurantappexample
import retrofit2.http.*

interface ApiService {
    @GET("restaurants")
    suspend fun getRestaurants(): MutableList<Restaurant>

    @GET("hits")
    suspend fun getHits(): MutableList<Hits>

    @GET("reviews")
    suspend fun getReviews(): MutableList<Reviews>
}