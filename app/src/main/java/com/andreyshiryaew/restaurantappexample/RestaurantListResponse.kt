package com.andreyshiryaew.restaurantappexample

data class RestaurantListResponse<out T>(val ApiStatus: ApiStatus, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T): RestaurantListResponse<T> = RestaurantListResponse(ApiStatus = ApiStatus.SUCCESS, data = data, message = null)
        fun <T> error(data: T?, message: String): RestaurantListResponse<T> =
            RestaurantListResponse(ApiStatus = ApiStatus.ERROR, data = data, message = message)

        fun <T> loading(data: T?): RestaurantListResponse<T> = RestaurantListResponse(ApiStatus = ApiStatus.LOADING, data = data, message = null)
    }
}

enum class ApiStatus {
    SUCCESS,
    ERROR,
    LOADING
}