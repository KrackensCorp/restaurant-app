package com.andreyshiryaew.restaurantappexample.ui.screens.restaurant

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.andreyshiryaew.restaurantappexample.R
import com.andreyshiryaew.restaurantappexample.Restaurant
import java.util.*

@Composable // Отрисовка экрана ресторанов
fun RestaurantScreen(navController: NavController, listRestaurant: MutableList<Restaurant>, isLoading: Boolean) {
    val textState = remember { mutableStateOf(TextFieldValue("")) }
    Column {
        SearchView(textState)
        RestaurantList(navController = navController, state = textState, listRestaurant = listRestaurant, isLoading = isLoading)
    }
}

@Composable
fun RestaurantList(navController: NavController, state: MutableState<TextFieldValue>, listRestaurant: MutableList<Restaurant>, isLoading: Boolean) {
    val restaurants = listRestaurant
    var filteredRestaurants: MutableList<Restaurant>

    if (isLoading) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.fillMaxSize()
        ) { CircularProgressIndicator() }
    } else {
        LazyColumn { // Наполняем элементы списка ресторанов информацией
            val searchedText = state.value.text

            filteredRestaurants =
                if (searchedText.isEmpty()) {
                    restaurants
                } else {
                    var resultList = mutableListOf<Restaurant>()
                    for (restaurants in restaurants) {
                        if (restaurants.toString().lowercase(Locale.getDefault())
                                .contains(searchedText.lowercase(Locale.getDefault()))
                        ) {
                            resultList.add(restaurants)
                        }
                    }
                    resultList
                }

            items(filteredRestaurants) { filteredRestaurant ->
                RestaurantItemColumn(
                    item = filteredRestaurant,
                    onItemClick = { selectedRestaurant ->
                        navController.navigate("details/$selectedRestaurant") {
                            popUpTo("main") { saveState = true }
                            launchSingleTop = true
                            restoreState = true
                        }
                    }
                )
            }
        }
    }
}

@Composable // Окошко поиска вместо Топ Бара
fun SearchView(state: MutableState<TextFieldValue>) {
    TextField(
        value = state.value,
        onValueChange = { value ->
            state.value = value
        },
        modifier = Modifier
            .fillMaxWidth(),
        textStyle = TextStyle(color = Color.Black, fontSize = 18.sp),
        leadingIcon = {
            Icon(
                Icons.Default.Search,
                contentDescription = "",
                modifier = Modifier
                    .padding(15.dp)
                    .size(24.dp)
            )
        },
        trailingIcon = {
            if (state.value != TextFieldValue("")) {
                IconButton(
                    onClick = {
                        state.value =
                            TextFieldValue("") // Удаляем текст по крестику
                    }
                ) {
                    Icon(
                        Icons.Default.Close,
                        contentDescription = "",
                        modifier = Modifier
                            .padding(15.dp)
                            .size(24.dp)
                    )
                }
            }
        },
        singleLine = true,
        shape = RectangleShape,
        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.Black,
            cursorColor = Color.Black,
            leadingIconColor = Color.Black,
            trailingIconColor = Color.Black,
            backgroundColor = colorResource(id = R.color.white),
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        )
    )
}

@Composable // Делаем красивые карточки с ресторанами
fun RestaurantItemColumn(item: Restaurant, onItemClick: (String) -> Unit) {
    Card(
        modifier = Modifier
            .clickable { onItemClick(item.Name) }
            .fillMaxWidth()
            .padding(10.dp),
        shape = RoundedCornerShape(15.dp),
        elevation = 5.dp
    ) {
        Box {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberImagePainter(item.Logo),
                    contentDescription = "image",
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .padding(16.dp)
                        .size(64.dp)
                        .clip(CircleShape)
                        .background(Color.DarkGray)
                )
                Column(
                    modifier = Modifier.padding(start = 16.dp)
                ) {
                    Text(text = item.Name)
                    SpecializationTextBuilder(item)
                    PercentageReviewsBuilder(item)
                }
            }
        }
    }
}

@Composable // Экранчик с названием ресторана по нажатию на карточку ресторана
fun RestaurantDetailsScreen(restaurantName: String) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colorResource(id = R.color.white))
            .wrapContentSize(Alignment.Center)
    ) {
        Text(
            text = restaurantName,
            color = Color.Black,
            modifier = Modifier.align(Alignment.CenterHorizontally),
            textAlign = TextAlign.Center,
            fontSize = 22.sp
        )
    }
}

@Composable // Собираем строчку с процентом хороших отзывов на карточке
fun PercentageReviewsBuilder(item: Restaurant) {
    Row(
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = painterResource(id = R.drawable.thumb_up_outline),
            contentDescription = "image",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(16.dp)
        )
        Text(text = (" " + item.PositiveReviews.toString() + "%"))
    }
}

@Composable // Делаем из получиенных специализаций строчку с слешами
fun SpecializationTextBuilder(item: Restaurant) {
    var stringBuilder = ""
    for (i in 1..item.Specializations.size) {
        stringBuilder += item.Specializations[i - 1].Name
        if (i < item.Specializations.size) {
            stringBuilder += " / "
        } else {
            Text(text = stringBuilder)
        }
    }
}