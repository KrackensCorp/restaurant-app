package com.andreyshiryaew.restaurantappexample

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers

class RestaurantViewModel() : ViewModel() {
    fun getRestaurantData() = liveData(Dispatchers.IO) {
        emit(RestaurantListResponse.loading(data = null))
        try {
            emit(RestaurantListResponse.success(data = ApiClient.MY_API_SERVICE.getRestaurants()))
        } catch (exception: Exception) {
            emit(RestaurantListResponse.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

}