package com.andreyshiryaew.restaurantappexample

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Reviews(
    @SerializedName("IsPositive")
    val IsPositive: Boolean,

    @SerializedName("Message")
    val Message: String,

    @SerializedName("DateAdded")
    val DateAdded: String,

    @SerializedName("UserFIO")
    val UserFIO: String,

    @SerializedName("RestaurantName")
    val RestaurantName: String
) : Serializable